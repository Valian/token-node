import { Request, Response} from 'express'
import User, {IUser} from '../models/User'
import jwt from 'jsonwebtoken'

export const signtUp = async (req: Request, res: Response)=>{
    //saving user
    const user: IUser = new User({
        username    : req.body.username,
        password    : req.body.password,
        email       : req.body.email
    })

    user.password = await user.encryptPassword(user.password)
    const saveUser = await user.save()
    // token
    const token: string = jwt.sign({_id: saveUser._id},  `${process.env.TOKEN_SECRET}`)

    res.header('auth-token', token).json(saveUser)
}
export const signIn = async (req: Request, res: Response)=>{
    const user = await User.findOne({email: req.body.email})
    if(!user) return res.status(400).json('Email is wrong')

    const connectPass =  await user.validatePassword(req.body.password)

    if(!connectPass) return res.status(400).json('Invalid Password')

    const token = jwt.sign({_id: user._id}, `${process.env.TOKEN_SECRET}`, {
        expiresIn: 60 * 60 * 24 * 7
    })
    res.header('auth-token', token).json(user)    
}
export const profile = async (req: Request, res: Response)=>{
    const user = await User.findById(req.userId, {password : 0})
    if(!user) return res.status(404).json('no user found')
    res.json(user)
}