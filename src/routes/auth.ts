import { Router } from 'express'
import {tokenValidation} from '../libs/verifyToken'
const router : Router = Router();

import { signtUp, signIn, profile} from '../controllers/authController'

router.post('/signUp', signtUp)
router.post('/signIn', signIn)
router.get('/profile', tokenValidation, profile)


export default router;