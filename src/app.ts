import express, { Application } from 'express'
const app: Application = express()
import authRoutes from './routes/auth'
import morgan from 'morgan'


//settings
app.set('port', 3000)
//middleware
app.use(morgan('dev'))
app.use(express.json())
//routes
app.use('/api/auth',authRoutes)


export default app;