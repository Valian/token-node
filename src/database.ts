import db from 'mongoose'

db.connect('mongodb://localhost:27017/test', {
useNewUrlParser: true,
useUnifiedTopology: true,
useCreateIndex: true
})
    .then(db => console.log('Database is connected'))
    .catch(err => console.log(err))
