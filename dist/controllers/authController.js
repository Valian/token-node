"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = __importDefault(require("../models/User"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.signtUp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //saving user
    const user = new User_1.default({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    });
    user.password = yield user.encryptPassword(user.password);
    const saveUser = yield user.save();
    // token
    const token = jsonwebtoken_1.default.sign({ _id: saveUser._id }, `${process.env.TOKEN_SECRET}`);
    res.header('auth-token', token).json(saveUser);
});
exports.signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.default.findOne({ email: req.body.email });
    if (!user)
        return res.status(400).json('Email is wrong');
    const connectPass = yield user.validatePassword(req.body.password);
    if (!connectPass)
        return res.status(400).json('Invalid Password');
    const token = jsonwebtoken_1.default.sign({ _id: user._id }, `${process.env.TOKEN_SECRET}`, {
        expiresIn: 60 * 60 * 24 * 7
    });
    res.header('auth-token', token).json(user);
});
exports.profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.default.findById(req.userId, { password: 0 });
    if (!user)
        return res.status(404).json('no user found');
    res.json(user);
});
//# sourceMappingURL=authController.js.map