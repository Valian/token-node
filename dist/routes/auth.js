"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const verifyToken_1 = require("../libs/verifyToken");
const router = express_1.Router();
const authController_1 = require("../controllers/authController");
router.post('/signUp', authController_1.signtUp);
router.post('/signIn', authController_1.signIn);
router.get('/profile', verifyToken_1.tokenValidation, authController_1.profile);
exports.default = router;
//# sourceMappingURL=auth.js.map